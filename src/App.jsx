// React core
import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Components
import HomePage from "./components/templates/HomePage";
import ResultPage from "./components/templates/ResultPage";
import VideoPage from "./components/templates/VideoPage";

// Other imports
import "./css/style.css";
import information from "./information.json";

export default function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route
            path="/"
            exact
            render={() => <HomePage information={information} />}
          />
          <Route
            path="/video/:id"
            render={({ match }) => (
              <VideoPage match={match} information={information} />
            )}
          />
          <Route
            path="/results/:query"
            render={({ match }) => (
              <ResultPage match={match} information={information} />
            )}
          />
        </Switch>
      </div>
    </Router>
  );
}
