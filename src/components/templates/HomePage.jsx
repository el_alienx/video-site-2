// React core
import React from "react";

// Components
import Card from "../molecules/Card";
import Header from "../organisms/Header";

export default function HomePage({ information }) {
  // We render multiple components using map function
  const Cards = information.map((item) => {
    return <Card key={item.id} data={item} />;
  });

  return (
    <div className="home-page">
      <Header />
      <section className="recomended">
        <h2>Recommended videos</h2>
        <div className="grid">{Cards}</div>
      </section>
    </div>
  );
}
