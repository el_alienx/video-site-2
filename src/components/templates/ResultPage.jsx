// React core
import React, { useState } from "react";

// Components
import Card from "../molecules/Card";
import Header from "../organisms/Header";

export default function ResultPage({ match, information }) {
  // Data
  const query = new RegExp(match.params.query, "i");
  const results = information.filter((item) => item.title.match(query));

  const [cards, setCards] = useState(sortCards("title"));

  function sortCards(key) {
    const sortedResults = results.sort((a, b) => (a[key] > b[key] ? 1 : -1));

    return sortedResults.map((item) => <Card key={item.id} data={item} />);
  }

  return (
    <div className="result-page">
      <Header />

      <section className="container">
        <h1>Filter results by</h1>
        <button onClick={() => setCards(sortCards("title"))}>Name</button>
        <button onClick={() => setCards(sortCards("channel"))}>Channel</button>
        <hr />
        <div className="grid">{cards}</div>
      </section>
    </div>
  );
}
