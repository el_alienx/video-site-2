// React core
import React from "react";
import { Link } from "react-router-dom";

// Components
import Header from "../organisms/Header";

export default function VideoPage({ match, information }) {
  const matchId = match.params.id - 1;
  const {
    videoURL,
    title,
    views,
    uploadDate,
    channelThumb,
    channelName,
    suscribers,
    description,
  } = information[matchId];

  console.log("Video page");
  console.log(match);
  console.log(information);
  console.log(matchId);
  console.log(information[matchId]);

  return (
    <div className="video-page">
      <Header hasLightTheme={false} />

      <div className="video-container">
        {/* controls enable the play pause, etc otherwise the video appears with any buttons */}
        <video controls>
          <source src={videoURL} type="video/mp4" />
        </video>
      </div>

      <div className="body-container">
        <h1 className="title">{title}</h1>
        <p className="description">
          {views} views • {uploadDate}
        </p>
        <hr />
        <div className="meta-data">
          <div className="left">
            <img
              className="channel-thumb"
              src={channelThumb}
              alt="Channel thumbnail"
            />
          </div>
          <div className="right">
            <h1 className="title">{channelName}</h1>
            <p className="description">{suscribers} suscribers</p>
            <p className="description">{description}</p>
          </div>
        </div>
        <hr />

        {/* Replace with Link later */}
        <Link className="button" to="/">
          Back to home
        </Link>
      </div>
    </div>
  );
}
