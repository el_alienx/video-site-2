// React core
import React, { useState } from "react";
import { Link } from "react-router-dom";

// Media assets
import logoLight from "../../assets/images/logo-light.svg";
import logoDark from "../../assets/images/logo-dark.svg";

// hasLightTheme is boolean that will toogle the logo and css class
export default function Header({ hasLightTheme = true }) {
  // Data
  const color = hasLightTheme ? "light" : "dark";
  const logo = hasLightTheme ? logoLight : logoDark;

  // Reactive data
  const [query, setQuery] = useState("");

  return (
    // headerlight
    <header className={`header ${color}`}>
      <Link to="/">
        <img
          src={logo}
          title="Reactube logo"
          alt="A red playback button with the words Reactube written next to it"
        />
      </Link>

      <div className="search-bar">
        <input
          placeholder="Search"
          value={query}
          onChange={(event) => setQuery(event.target.value)}
        />

        <Link to={`/results/${query}`}>Search</Link>
      </div>
    </header>
  );
}
