// React core
import React from "react";
import { Link } from "react-router-dom";

export default function Card({ data }) {
  // Here is where we do the break down the data
  const {
    id,
    title,
    description,
    channelName,
    channelThumb,
    views,
    videoThumb,
  } = data;

  return (
    <article className="card">
      {/* We going to explain Link later */}
      <Link to={`video/${id}`}>
        <img className="thumbnail" src={videoThumb} alt={description}></img>
      </Link>

      <aside className="meta-data">
        <div className="left">
          <img
            className="channel-thumb"
            src={channelThumb}
            alt="Channel thumbnail"
          />
        </div>
        <div className="right">
          <h3 className="title">{title}</h3>
          <p className="description">{channelName}</p>
          <p className="description">{views} views</p>
        </div>
      </aside>
    </article>
  );
}
